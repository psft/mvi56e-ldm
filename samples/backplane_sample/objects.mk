################################################################################
# List of libraries used in Application
################################################################################

LIBS := \
rt		\
ocxcnapi \
ocxbpeng \
ocxbpapi 


SYSLIBS := 

LIBDIRS := \
ocx

# target to be built ( include extension for libraries )

TARGETAPP := BackPlane_Sample


#  source files that are part of the application proper

SRCS_C = 	\
backplane_sample.c

